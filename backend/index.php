<?php
session_start();
// khai báo thời gian web
date_default_timezone_set("Asia/Ho_Chi_Minh");
// xử lý url trên trình duyệt để nhúng được class
//controller tương ứng,khởi tạo class và gọi action tương ứng
//lấy ra tham số controller và action từ trình duyệt
$controller = isset($_GET['controller']) ? $_GET['controller'] : 'category';
$action = isset($_GET['action']) ? $_GET['action'] : 'index';
$controller = ucfirst($controller);
$controller .= "Controller";
$path_controller = "controllers/$controller.php";

//kiểm tra nếu đường dẫn ko tồn tại, thì báo trang ko tồn tại
if (!file_exists($path_controller)) {
  die('Trang bạn tìm không tồn tại');
}

require_once "$path_controller";

//khởi tạo đối tượng sau khi nhúng file
$object = new $controller();

if (!method_exists($object, $action)) {
  die("Không tồn tại phương thức $action của class $controller");
}

$object->$action();
