<div class="container" style="max-width: 500px">
    <form method="post" action="">
        <img src="https://canifa.com/assets/images/logo.svg" alt="">
        <h2>Đăng Nhập Tài Khoản</h2>
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="username" value="" id="username" class="form-control" />
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" value="" id="password" class="form-control" />
        </div>
        <div class="form-group">
            <input type="submit" name="submit" value="Đăng nhập" class="btn btn-primary" />
            <p>
                Chưa có tài khoản, <a href="index.php?controller=customer&action=register">Đăng ký ngay</a>
            </p>
        </div>
    </form>
</div>